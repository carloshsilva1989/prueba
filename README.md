# prueba

#Instrucciones: 

1) Clonar de la siguiente dirección: 
	git clone https://carloshsilva1989@bitbucket.org/carloshsilva1989/prueba.git

o en su defecto desempaquetar el comprimido prueba.zip en la carpeta /var/www/html/ ( en ubuntu 16.04 )

2) Entrar al directorio /var/www/hmtl/prueba/backend ( en ubuntu 16.04 ) a travez de terminal y correr el comando composer install.

3) Crear un archivo .env ( si se clono del repositorio bitbucket ) y clonar el contenido ubicado en .env.example. pero si se descomprimio del prueba.zip obviar este paso. 

4) Crear una base de datos llamada "prueba"

5) Modificar los datos del .env con los datos de acceso a la base de datos DB_DATABASE, DB_USERNAME, DB_PASSWORD.

6) Ejecutar en terminal "php artisan migrate"

7) Ejecutar en terminal "php artisan jwt:secret"

8) Dar permisos de ejecución lectura y escritura a la carpeta "storage" ( en caso de ser ubuntu o mac ). 

9) Entrar en el directorio /var/www/html/prueba/frontend y ejecutar el comando "cordova run"

10) Registrarse -> iniciar sesión -> editar perfil.


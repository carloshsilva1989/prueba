<?php
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:api', ['except' => ['login']]);
		$this->middleware('jwt', ['except' => ['login','test','register']]);
    }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function test()
    {
    	return 'hola mundo';
    }

    public function register(Request $request)
    {
        $data = $request->json()->all();
        $user = new User;

        if (!$user->userExist($data['email']))
        {
                
            $user_register = User::create([
                'name'      => $data['name'],
                'email'     => $data['email'],
                'password'  => Hash::make($data['password']),
            ]);

            return response()->json(['Usuario registrado con exito!']);
        }
        else
        {
            return response()->json(['status'=>'User already exists!'] , 401 , []);
        }
    }

    public function update(Request $request)
    {
        $data = $request->json()->all();

        $user = User::where('email',auth()->user()->email)->first();

        if($user)
        {
            $user->name = $data['name'];

            if($data['password'])
            {
                $user->password = Hash::make($data['password']);
            }

            $user->save();

            return response()->json(['status' => 'Updated user data!'], 200);
        }
        else
        {
            return response()->json(['status' => 'Dont exits user!'], 400);
        }
    }

    public function login()
    {
        $credentials = request(['email', 'password']);
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }
    public function payload()
    {
        return response()->json(auth()->payload());
    }
    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }
    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user(),
        ]);
    }
}
myApp.controller('updateProfileController',['$scope','$http','$localStorage','$location',function($scope,$http,$localStorage,$location){

    
    
    $scope.visible = false;

    if($localStorage.auth !== undefined && $localStorage.auth.access_token){
        $scope.visible = true;
        $scope.user = {
            name : $localStorage.auth.user.name,
            password : null,
            password2 : null
        }
    }

    if($localStorage.auth === undefined){
        $location.path("/home");
    }
    
    $scope.updateProfile = function(){
         
        if(!$scope.user.name){
            toastr.error('Debes introducir tu nombre y apellido', 'Error')
            return false;
        }

        if( $scope.user.password || $scope.user.password2 ){
            if(!passwords_equals($scope.user.password,$scope.user.password2)){
                toastr.error('Las contraseñas no son iguales', 'Error')
                return false
            }    
        }

        $http({
            method: 'PUT',
            url: url('auth/update'),
            headers: {
                'Authorization' : "Bearer " + $localStorage.auth.access_token
              },
            data : {
                ...$scope.user
            }
          }).then(function successCallback(response) {

            $localStorage.auth.user.name = $scope.user.name
                toastr.success('Tus datos han sido actualizados satisfatoriamente', 'Todo ok')
            }, function errorCallback(response) {

                // No se solicito validar la respuesta o codigos de errores del servidor

            });
    }
}])
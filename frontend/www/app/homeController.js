myApp.controller('homeController',['$scope','$localStorage','$location','$http',function($scope,$localStorage,$location,$http){

    $scope.displayButton = false;

    if($localStorage.auth && $localStorage.auth.access_token){
        $scope.displayButton = true;
    }

    $scope.logout = function(){

        $http({
            method: 'POST',
            url: url('auth/logout'),
            headers: {
                'Authorization' : "Bearer " + $localStorage.auth.access_token
              },
            data : {
                
            }
          }).then(function successCallback(response) {

            $localStorage.$reset();
            $scope.displayButton = false;
            $location.path("/home");
            

            }, function errorCallback(response) {

                // No se solicito validar la respuesta o codigos de errores del servidor

            });
    }
}])
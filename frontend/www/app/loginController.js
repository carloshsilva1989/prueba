myApp.controller('loginController',['$scope','$http','$localStorage','$location',function($scope,$http,$localStorage,$location){

    $scope.user = {
        email : 'elmorochez22@gmail.com',
        password : '123456789'
    }
    
    $scope.visible = false;

    if($localStorage.auth && $localStorage.auth.access_token){
        $location.path("/home");
    }else{

        $scope.visible = true;
    }

    $scope.loginAttemp = function(){


        if(!$scope.user.email){
            toastr.error('Debes introducir tu correo electronico', 'Error')
            return false;
        }

        if(!$scope.user.password){
            toastr.error('Debes introducir tu clave de acceso', 'Error')

            return false;
        }

        $http({
            method: 'POST',
            url: url('auth/login'),
            data : {
                ...$scope.user
            }
          }).then(function successCallback(response) {

                $localStorage.auth = {...response.data}
                $location.path("/update-profile");
                toastr.success('Iniciaste sesion con exito', 'Todo ok')
            }, function errorCallback(response) {
                toastr.error('Usuario o clave incorrecta', 'Error')
                $scope.user.password = ''
                // No se solicito validar la respuesta o codigos de errores del servidor

            });
    }

}])
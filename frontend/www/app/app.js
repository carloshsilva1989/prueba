
var myApp = angular.module('myApp',['ngRoute','ngStorage']);

myApp.config(['$routeProvider',function($routeProvider){

    $routeProvider
    .when('/home',{
        templateUrl : 'views/homeScreen.html',
        controller : 'homeController'
    })
    .when('/login',{
        templateUrl : 'views/loginScreen.html',
        controller : 'loginController'
    })
    .when('/register',{
        templateUrl : 'views/registerScreen.html',
        controller : 'registerController'
    })
    .when('/update-profile',{
        templateUrl : 'views/updateProfileScreen.html',
        controller : 'updateProfileController'
    })
    .otherwise({
        redirectTo : '/home'
    })
}])


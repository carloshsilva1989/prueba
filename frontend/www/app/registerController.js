myApp.controller('registerController',['$scope','$http','$localStorage','$location',function($scope,$http,$localStorage,$location){

    $scope.user = {
        name        : 'Carlos Silva',
        email       : 'carloshsilva1989@gmail.com',
        password   : '123456789',
        password2   : '123456789',
    }

    if($localStorage.auth && $localStorage.auth.access_token){
        $location.path("/update-profile");
    }

    $scope.registerAttepm = function(){
        
        if(!$scope.user.name){
            toastr.error('Debes introducir tu nombre y apellido', 'Error')
            return false;
        }

        if(!$scope.user.email){
            toastr.error('Debes introducir tu correo electronico', 'Error')
            return false;
        }

        if(!$scope.user.password){
            toastr.error('Debes introducir tu clave', 'Error')
            return false;
        }

        if(!$scope.user.password2){
            toastr.error('Debes introducir tu clave de confirmacion', 'Error')
            return false;
        }


        if(!passwords_equals($scope.user.password,$scope.user.password2)){
            toastr.error('Las contraseñas no son iguales', 'Error')
            return false;
        }

            $http({
                method: 'POST',
                url: url('auth/register'),
                data : {
                    ...$scope.user
                }
            }).then(function successCallback(response) {

                //alert('Registro satisfactorio');
                toastr.success('Registro con exito! Puede proseguir a iniciar sesión.', 'Todo ok')
                $location.path("/login");

            }, function errorCallback(response) {

                toastr.error('Este usuario ya se encuentra registrado', 'Error')

            });
    }

}])